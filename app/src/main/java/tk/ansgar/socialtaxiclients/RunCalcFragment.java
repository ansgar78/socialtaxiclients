package tk.ansgar.socialtaxiclients;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RunCalcFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RunCalcFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener{

    private static final String TAG = "RunCalcFrag";

    private OnFragmentInteractionListener mListener;

    private SupportMapFragment mapFragment;
    private UiSettings mMapSettings;
    private GoogleMap mGMap;

    private AutoCompleteTextView mOrigin;
    private AutoCompleteTextView mDestination;
    private TextInputEditText mDate;
    private TextInputEditText mTime;

    private DatePickerDialog mDatePicker;
    private TimePickerDialog mTimePicker;

    private Calendar selectedDate;

    private GoogleApiClient googleApiClient;
    private PlacesAutocompleteAdapter placesAutocompleteAdapter;

    private final OkHttpClient http = new OkHttpClient();

    private static final LatLngBounds BOUNDS_CATALUNYA = new LatLngBounds(
            new LatLng(39.5050288,-3.1360701),
            new LatLng(42.9335013, 2.0115475));

    private Marker marker_start_previous;
    private Marker marker_end_previous;
    private Polyline route_previous;

    public final static List<LatLng> AEROPORT_DEL_PRAT = new ArrayList<>();
    public final static List<LatLng> MOLL_ADOSSAT = new ArrayList<>();
    public final static List<GregorianCalendar> DIES_FESTIUS = new ArrayList<>();
    public final static List<GregorianCalendar> SPECIAL_NIGHTS = new ArrayList<>();

    public enum RunConcepts{
        MinimumFare (2.10, 2.10, 2.30, 39.00),
        Km          (1.10, 1.30, 1.40, 00.00),
        Reserve     (4.50, 4.50, 4.50, 00.00);

        private final double t1;
        private final double t2;
        private final double t3;
        private final double t4;

        RunConcepts(double t1, double t2, double t3, double t4){
            this.t1 = t1;
            this.t2 = t2;
            this.t3 = t3;
            this.t4 = t4;
        }

        public double getPrice(int t){
            switch (t){
                case 1: return t1;
                case 2: return t2;
                case 3: return t3;
                case 4: return t4;
                default: throw new InvalidParameterException("Tariff must be one of [1,2,3,4]");
            }
        }
    }

    static {
        AEROPORT_DEL_PRAT.add(new LatLng(41.2799996,2.0753860)); // 1
        AEROPORT_DEL_PRAT.add(new LatLng(41.2909639,2.1081734)); // 2
        AEROPORT_DEL_PRAT.add(new LatLng(41.3064397,2.1121216)); // 3
        AEROPORT_DEL_PRAT.add(new LatLng(41.3192046,2.1042251)); // 4
        AEROPORT_DEL_PRAT.add(new LatLng(41.3086318,2.0645714)); // 5
        AEROPORT_DEL_PRAT.add(new LatLng(41.3046344,2.0541000)); // 6
        AEROPORT_DEL_PRAT.add(new LatLng(41.2991535,2.0489500)); // 7
        AEROPORT_DEL_PRAT.add(new LatLng(41.2918667,2.0492935)); // 8
        AEROPORT_DEL_PRAT.add(new LatLng(41.2799996,2.0753860)); // 9

        MOLL_ADOSSAT.add(new LatLng(41.3657943,2.1819878)); // 1
        MOLL_ADOSSAT.add(new LatLng(41.3661848,2.1809148)); // 2
        MOLL_ADOSSAT.add(new LatLng(41.3648360,2.1800993)); // 3
        MOLL_ADOSSAT.add(new LatLng(41.3643285,2.1814724)); // 4
        MOLL_ADOSSAT.add(new LatLng(41.3522332,2.1737480)); // 5
        MOLL_ADOSSAT.add(new LatLng(41.3513957,2.1765804)); // 6
        MOLL_ADOSSAT.add(new LatLng(41.3634430,2.1842623)); // 7
        MOLL_ADOSSAT.add(new LatLng(41.3652145,2.1838760)); // 8
        MOLL_ADOSSAT.add(new LatLng(41.3657943,2.1819878)); // 9

        // This special dates last 8h.
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.DECEMBER , 26, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.JANUARY  ,  2, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.JANUARY  ,  7, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.FEBRUARY , 13, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.MARCH    , 26, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.MARCH    , 27, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.MAY      , 17, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.JUNE     , 25, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.AUGUST   , 16, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.SEPTEMBER, 25, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.NOVEMBER ,  2, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.DECEMBER ,  7, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.DECEMBER ,  9, 0, 0));
        DIES_FESTIUS.add(new GregorianCalendar(2016, Calendar.DECEMBER , 27, 0, 0));

        // This special nights last 12h.
        SPECIAL_NIGHTS.add(new GregorianCalendar(2016, Calendar.JULY     , 23, 20, 0));
        SPECIAL_NIGHTS.add(new GregorianCalendar(2016, Calendar.DECEMBER , 24, 20, 0));
        SPECIAL_NIGHTS.add(new GregorianCalendar(2016, Calendar.DECEMBER , 23, 20, 0));
    }

    public RunCalcFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RunCalcFragment.
     */
    public static RunCalcFragment newInstance() {
        RunCalcFragment fragment = new RunCalcFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        placesAutocompleteAdapter = new PlacesAutocompleteAdapter(
                getContext(),
                googleApiClient,
                BOUNDS_CATALUNYA,
                null);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_run_calc, container, false);

        if(getChildFragmentManager().findFragmentByTag("map_tag") == null){
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.child_fragment_container, new SupportMapFragment(), "map_tag")
                    .commit();
        }

        selectedDate = Calendar.getInstance();
        selectedDate.add(Calendar.MINUTE, 10);

        mDatePicker = new DatePickerDialog(getContext(), this,
                selectedDate.get(Calendar.YEAR),
                selectedDate.get(Calendar.MONTH),
                selectedDate.get(Calendar.DAY_OF_MONTH));

        mTimePicker = new TimePickerDialog(getContext(), this,
                selectedDate.get(Calendar.HOUR_OF_DAY),
                selectedDate.get(Calendar.MINUTE),
                true);


        mOrigin = (AutoCompleteTextView) v.findViewById(R.id.origin);
        mDestination = (AutoCompleteTextView) v.findViewById(R.id.destination);
        mDate = (TextInputEditText) v.findViewById(R.id.date);
        mTime = (TextInputEditText) v.findViewById(R.id.time);

        mOrigin.setAdapter(placesAutocompleteAdapter);
        mOrigin.setOnItemClickListener(mAutocompleteClickListener);

        mDestination.setAdapter(placesAutocompleteAdapter);
        mDestination.setOnItemClickListener(mAutocompleteClickListener);

        mDatePicker.getDatePicker().setMinDate(selectedDate.getTime().getTime());

        onDateSet(null,
                selectedDate.get(Calendar.YEAR),
                selectedDate.get(Calendar.MONTH),
                selectedDate.get(Calendar.DAY_OF_MONTH));

        onTimeSet(null,
                selectedDate.get(Calendar.HOUR_OF_DAY),
                selectedDate.get(Calendar.MINUTE));

        v.findViewById(R.id.layout_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getContext());
                mDatePicker.show();
            }
        });
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getContext());
                mDatePicker.show();
            }
        });
        mDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    hideKeyboard(getContext());
                    mDatePicker.show();
                }
            }
        });

        v.findViewById(R.id.layout_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getContext());
                mTimePicker.show();
            }
        });
        mTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    hideKeyboard(getContext());
                    mTimePicker.show();
                }
            }
        });
        mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getContext());
                mTimePicker.show();
            }
        });


        return v;
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final AutocompletePrediction item = placesAutocompleteAdapter.getItem(position);
            final String placeID = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeID);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            hideKeyboard(getContext());
            tryShowRouteOnMap();
        }
    };

    private void tryShowRouteOnMap() {
        String origin = mOrigin.getText().toString();
        String destination = mDestination.getText().toString();

        if(origin.isEmpty() || destination.isEmpty())
            return;

        if(origin.equalsIgnoreCase(getString(R.string.my_position)))
            origin = "Mollet del Vallés, Barcelona"; // TODO: Find user's actual position

        Uri api_request = new Uri.Builder()
                .scheme("https")
                .authority("maps.googleapis.com")
                .path("maps/api/directions/json")
                .appendQueryParameter("key", "AIzaSyCTtQH1euImGcFmtYEYqATXydKug4bqs0I")
                .appendQueryParameter("origin", origin)
                .appendQueryParameter("destination", destination)
                .appendQueryParameter("mode", "driving").build();

        new AsyncTask<String, Void, String>(){
            @Override
            protected String doInBackground(String... params) {
                Request request = new Request.Builder()
                        .url(params[0])
                        .build();

                try {
                    return http.newCall(request).execute().body().string();
                } catch (IOException e){
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject route = new JSONObject(s)
                            .getJSONArray("routes")
                            .getJSONObject(0);

                    JSONObject northeast_bound = route
                            .getJSONObject("bounds")
                            .getJSONObject("northeast");

                    JSONObject southwest_bound = route
                            .getJSONObject("bounds")
                            .getJSONObject("southwest");

                    String polylineEncoded = route
                            .getJSONObject("overview_polyline")
                            .getString("points");

                    LatLngBounds overview_bounds = new LatLngBounds(
                            new LatLng(
                                    southwest_bound.getDouble("lat"),
                                    southwest_bound.getDouble("lng")
                            ),
                            new LatLng(
                                    northeast_bound.getDouble("lat"),
                                    northeast_bound.getDouble("lng")
                            )
                    );

                    double routeMeters = route
                            .getJSONArray("legs")
                            .getJSONObject(0)
                            .getJSONObject("distance")
                            .getInt("value");

                    List<LatLng> points = PolyUtil.decode(polylineEncoded);
                    PolylineOptions polyOptions = new PolylineOptions()
                            .width(8f)
                            .color(Color.BLUE);

                    for (LatLng point : points) {
                        polyOptions.add(point);
                    }

                    if(marker_end_previous != null)
                        marker_end_previous.remove();

                    if(marker_start_previous != null)
                        marker_start_previous.remove();

                    if(route_previous != null)
                        route_previous.remove();

                    LatLng startingPoint = points.get(0);
                    LatLng endPoint = points.get(points.size() - 1);

                    MarkerOptions marker_start = new MarkerOptions().position(startingPoint);
                    MarkerOptions marker_end = new MarkerOptions().position(endPoint);

                    double runPrice = calcPrice(startingPoint, endPoint, routeMeters/1000d, selectedDate);

                    IconGenerator mIconGenerator = new IconGenerator(getContext());
                    mIconGenerator.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                    mIconGenerator.setStyle(IconGenerator.STYLE_BLUE);

                    Bitmap endIcon = mIconGenerator.makeIcon(NumberFormat.getCurrencyInstance().format(runPrice));

                    marker_end.icon(BitmapDescriptorFactory.fromBitmap(endIcon));

                    // TODO: Make waypoints draggable?
                    //marker_start_previous = mGMap.addMarker(marker_start);
                    marker_end_previous = mGMap.addMarker(marker_end);
                    route_previous = mGMap.addPolyline(polyOptions);
                    mGMap.animateCamera(CameraUpdateFactory
                            .newLatLngBounds(overview_bounds, 150));

                } catch (JSONException e) {
                    e.printStackTrace();
                    View view = getView();
                    if(view != null)
                        Snackbar.make(getView(), R.string.no_route_found, Snackbar.LENGTH_LONG).show();
                }
            }
        }.execute(api_request.toString());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        java.util.Formatter formatter = new java.util.Formatter();
        mDate.setText(formatter.format("%d/%d/%d", year, monthOfYear, dayOfMonth).toString());

        selectedDate.set(Calendar.YEAR, year);
        selectedDate.set(Calendar.MONTH, monthOfYear);
        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        tryShowRouteOnMap();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        java.util.Formatter formatter = new java.util.Formatter();
        mTime.setText(formatter.format("%02d:%02d", hourOfDay, minute).toString());

        selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDate.set(Calendar.MINUTE, minute);

        tryShowRouteOnMap();
    }

    public static void hideKeyboard(Context ctx) {
    InputMethodManager inputManager = (InputMethodManager) ctx
    .getSystemService(Context.INPUT_METHOD_SERVICE);

    // check if no view has focus:
     View v = ((Activity) ctx).getCurrentFocus();
     if (v == null)
        return;

    inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
 }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            places.release();
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        if(mapFragment == null) { // Prevent resetting mapFragment from the backstack.
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentByTag("map_tag");
            mapFragment.onResume();
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap gmap) {
        mGMap = gmap;

        mMapSettings = gmap.getUiSettings();
        mMapSettings.setMapToolbarEnabled(false);
        mMapSettings.setMyLocationButtonEnabled(false);
        mMapSettings.setZoomControlsEnabled(false);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public int calcTariff(LatLng orig, LatLng dest, Calendar datetime){
        // T1 - Workdays from 08:00-20:00.
        // T2 - Workdays  20:00-08:00, weekends/holidays from 06:-00-20:00, Mondays and (workdays after a holiday) from 00:00-08:00.
        // T3 - Weekends/holidays 00:00-06:00 and 20:00-24:00
        // T4 - Fixed rate between the airport and the cruise terminals.

        if((PolyUtil.containsLocation(orig, AEROPORT_DEL_PRAT, true) && PolyUtil.containsLocation(dest, MOLL_ADOSSAT, true)) ||
            PolyUtil.containsLocation(dest, AEROPORT_DEL_PRAT, true) && PolyUtil.containsLocation(orig, MOLL_ADOSSAT, true))
            return 4;

        for (GregorianCalendar dia_festiu : DIES_FESTIUS) {
            GregorianCalendar after = (GregorianCalendar) dia_festiu.clone();
            after.add(Calendar.HOUR_OF_DAY, 8);

            if(datetime.after(dia_festiu) && datetime.before(after)){
                return 3;
            }
        }

        switch (datetime.get(Calendar.DAY_OF_WEEK)){
            case Calendar.SATURDAY:
            case Calendar.SUNDAY:
                if ((datetime.get(Calendar.HOUR_OF_DAY) >=  0 &&
                     datetime.get(Calendar.HOUR_OF_DAY) <=  6)||
                    (datetime.get(Calendar.HOUR_OF_DAY) >= 20 &&
                     datetime.get(Calendar.HOUR_OF_DAY) <= 24)){
                    return 3;
                }
                break;
            default:
                if (datetime.get(Calendar.HOUR_OF_DAY) >= 8 &&
                    datetime.get(Calendar.HOUR_OF_DAY) <= 20){
                    return 1;
                }
                break;
        }

        return 2;

    }

    public double calcPrice(LatLng origin, LatLng destination, double km, Calendar datetime){

        double price = 0.0;
        int tariff = calcTariff(origin, destination, datetime);

        price += RunConcepts.MinimumFare.getPrice(tariff);
        price += RunConcepts.Km.getPrice(tariff) * km;
        price += RunConcepts.Reserve.getPrice(tariff);

        if(price <= 7d)
            return 7d;

        return price;
    }

}
