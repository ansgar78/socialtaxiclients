package tk.ansgar.socialtaxiclients.models;

import android.net.Uri;

import java.util.HashMap;
import java.util.Map;

import tk.ansgar.socialtaxiclients.R;

public class Driver {

    private String id;
    private String googleId;
    private String description;
    private double rating;
    private String surname;
    private Location location;
    private String name;
    private String phone;
    private String status;
    private Status _status;
    private Map<String, Object> additionalProperties = new HashMap<>();

    public enum Status{
        NOT_WORKING,
        IDLE,
        IN_RUN,
        DELEGATING;

        public static Status fromString(String string){
            switch (string){
                case "NOT_WORKING":
                    return NOT_WORKING;
                case "IDLE":
                    return IDLE;
                case "IN_RUN":
                    return IN_RUN;
                case "DELEGATING":
                    return DELEGATING;
                default:
                    return NOT_WORKING;
            }
        }

        public int getStringResourceId() {
            switch (this){
                case NOT_WORKING:
                    return R.string.driver_status_not_working;
                case IDLE:
                    return R.string.driver_status_idle;
                case IN_RUN:
                    return R.string.driver_status_in_run;
                case DELEGATING:
                    return R.string.driver_status_delegating;
                // Never reached
                default:
                    return 0;
            }
        }
    }

    public String getId() {
        return id;
    }
    public String getGoogleId() {
        return googleId;
    }
    public String getDescription() {
        return description;
    }
    public double getRating() {
        return rating;
    }
    public String getSurname() {
        return surname;
    }
    public Location getLocation() {
        return location;
    }
    public String getName() {
        return name;
    }
    public String getPhone() {
        return phone;
    }
    public Uri getPhoneUri() {
        return Uri.fromParts("tel", getPhone(), null);
    }
    public String getStatus() {
        return status;
    }
    public Status getStatusEnum() {
        return Status.fromString(status);
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setRating(double rating) {
        this.rating = rating;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setLocation(Location location) {
        this.location = location;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}