package tk.ansgar.socialtaxiclients.models;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ATaxiEndpointsInterface {

    @POST("api/v1/auth")
    Call<AuthToken> auth(
            @Body AuthToken authRequest);

    @GET("api/v1/drivers/{id}")
    Call<Driver> getDriver(
            @Path("id") String driver_id);

    @GET("api/v1/users/{id}")
    Call<User> getUser(
            @Path("id") String driver_id);

    @GET("api/v1/drivers/{page_num}")
    Call<ATaxiList<Driver>> getDrivers(
            @Path("page_num") int page_num);

    @POST("api/v1/drivers/new")
    Call<Driver> register_driver(
            @Body Driver driver);

    @GET("api/v1/drivers/nearby/{lat},{lng}")
    Call<ATaxiList<Driver>> getNearbyDrivers(
            @Path("lat") double lat,
            @Path("lng") double lng);

    @POST("api/v1/users/new")
    Call<User> register(
            @Body User user);

}
