package tk.ansgar.socialtaxiclients;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.ansgar.socialtaxiclients.models.ATaxiList;
import tk.ansgar.socialtaxiclients.models.Driver;

public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.DriverViewHolder> {

    private static final String TAG = "DriverListAdapter";

    List<Driver> driverList = new ArrayList<>();
    Resources resources;

    public DriverListAdapter(Resources resources){
        this.resources = resources;
    }

    public class DriverViewHolder extends RecyclerView.ViewHolder {

        protected TextView mName;
        protected TextView mStatus;
        protected ImageView mPhone;
        protected ImageView mPhoto;
        protected Context context;

        public DriverViewHolder(View v) {
            super(v);

            mName  = (TextView) v.findViewById(R.id.name);
            mStatus = (TextView) v.findViewById(R.id.status);
            mPhoto = (ImageView) v.findViewById(R.id.profile_pic);
            mPhone = (ImageView) v.findViewById(R.id.phone);

            context = v.getContext();
        }
    }

    @Override
    public DriverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_driver, parent, false);

        return new DriverViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DriverViewHolder holder, int position) {

        Bitmap stockUserPhoto = BitmapFactory.decodeResource(resources, R.mipmap.stock_user_profile);

        final tk.ansgar.socialtaxiclients.models.Driver d = driverList.get(position);
        holder.mName.setText(d.getName());
        holder.mStatus.setText(holder.context.getString(d.getStatusEnum().getStringResourceId()));
        holder.mPhoto.setImageDrawable(MainActivity.getRoundedBitmap(resources, stockUserPhoto));

        holder.mPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.getContext().startActivity(new Intent(Intent.ACTION_DIAL, d.getPhoneUri()));
            }
        });

    }

    public void updateDriverList(){

        Log.d(TAG, "Updating driver list.");

        Call<ATaxiList<Driver>> call = ATaxi.getInstance().getApi().getDrivers(1);
        call.enqueue(new Callback<ATaxiList<Driver>>() {
            @Override
            public void onResponse(Call<ATaxiList<Driver>> call, Response<ATaxiList<Driver>> response) {
                Log.d(TAG, "Driver list updated correctly.");
                driverList = response.body().getItems();
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ATaxiList<Driver>> call, Throwable t) {
                Log.d(TAG, "Couldn't update driver list.");
                t.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return driverList.size();
    }
}
