package tk.ansgar.socialtaxiclients;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.ansgar.socialtaxiclients.models.User;

public class RegisterUserActivity extends AppCompatActivity {

    public static final String USER_ID = "USER_ID";

    private TextInputEditText mName;
    private TextInputEditText mSurname;
    private TextInputEditText mPhone;
    private Button mRegister;

    private String userId;
    private String userName;
    private String userSurname;
    private String userPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        mName    = (TextInputEditText) findViewById(R.id.name);
        mSurname = (TextInputEditText) findViewById(R.id.surname);
        mPhone   = (TextInputEditText) findViewById(R.id.phone);
        mRegister= (Button) findViewById(R.id.register);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName    = mName   .getText().toString();
                userSurname = mSurname.getText().toString();
                userPhone   = mPhone  .getText().toString();

                if (!(userName   .equals("")  ||
                      userSurname.equals("")  ||
                      userPhone  .equals("")) ){

                    User user = new User();

                    user.setId(userId);
                    user.setName(userName);
                    user.setSurname(userSurname);
                    user.setPhone(userPhone);

                    ATaxi.getInstance().setUser(user);

                    Call<User> call = ATaxi.getInstance().getApi().register(ATaxi.getInstance().getUser());
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            ATaxi.getInstance().setUser(response.body());
                            finish();
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });

                    }
            }
        });

        Intent intent = getIntent();
        userId = intent.getStringExtra(USER_ID);

    }
}
