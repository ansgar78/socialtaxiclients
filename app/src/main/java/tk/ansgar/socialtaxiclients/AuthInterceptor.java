package tk.ansgar.socialtaxiclients;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import tk.ansgar.socialtaxiclients.models.AuthToken;

public class AuthInterceptor implements Interceptor {
    private static final String TAG = "AuthInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request req = chain.request();
        Response res = chain.proceed(req);

        return handleErrors(req, res, chain);
    }

    public static Response handleErrors(Request req, Response res, Chain chain) throws IOException {
        switch (res.code()){
            case 200:
                return res;
            case 401: // Invalid token
                throw new IOException("Please re-authenticate with google account.");
            case 403: // Invalid session
                Log.d(TAG, "403 received, trying to re-authenticate.");

                if(ATaxi.getInstance().getGoogleTok() != null){
                    return requestATaxiAuth(req, chain);
                } else {
                    Log.d(TAG, "Can't re-authenticate, google token is missing.");
                    Log.d(TAG, "Trying to get cached google token");

                    return requestGoogleAuth(req, chain);
                }

            default:
                throw new IOException("Unkown response code while calling api: " + res.code() + "\n" + res.body().string());
        }
    }

    public static Response requestATaxiAuth(Request request, Chain chain) throws IOException {
        AuthToken auth = new AuthToken();
        auth.setToken(ATaxi.getInstance().getGoogleTok());

        Call<AuthToken> call = ATaxi.getInstance().getApi().auth(auth);
        retrofit2.Response<AuthToken> authResponse = call.execute();

        ATaxi.getInstance().setSessionToken(authResponse.body().getToken());

        Request newReq = AuthHeadersInterceptor.addHeaders(request);

        return handleErrors(
                AuthHeadersInterceptor.addHeaders(newReq),
                chain.proceed(newReq),
                chain);
    }

    public static Response requestGoogleAuth(Request req, Chain chain) throws IOException {
        if(ATaxi.getInstance().getContext() != null){
            Context context = ATaxi.getInstance().getContext();

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.server_client_id))
                    .requestEmail()
                    .build();

            GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            try {
                ConnectionResult connRes = mGoogleApiClient.blockingConnect(10, TimeUnit.SECONDS);

                if(connRes.isSuccess()){
                    OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                    if(opr.isDone()){
                        GoogleSignInResult result = opr.get();

                        if(result.isSuccess()){
                            Log.d(TAG, "Got google token from cache");
                            GoogleSignInAccount acc = result.getSignInAccount();
                            ATaxi.getInstance().setGoogleTok(acc.getIdToken());
                            ATaxi.getInstance().setGoogleId(acc.getId());

                            return requestATaxiAuth(req, chain);
                        } else {
                            Log.d(TAG, "Can't get cached google token, context has not been set.");
                            context.startActivity(new Intent(context, LoginActivity.class));
                        }
                    }
                }
            } finally {
                mGoogleApiClient.disconnect();
            }
        }
        throw new IOException("Context not set.");
    }
}