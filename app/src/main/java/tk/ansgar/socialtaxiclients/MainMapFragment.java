package tk.ansgar.socialtaxiclients;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.ansgar.socialtaxiclients.models.ATaxiList;
import tk.ansgar.socialtaxiclients.models.Driver;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainMapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "MainMapFragment";

    private SupportMapFragment mapFragment;
    private UiSettings mMapSettings;
    private GoogleMap mGmap;
    private List<Marker> mDriverMarkers = new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    private List<Driver> nearbyDrivers = new ArrayList<>();

    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            Call<ATaxiList<Driver>> call = ATaxi.getInstance().getApi().getNearbyDrivers(41, 2);

            Log.i(TAG, "Polling nearby drivers");

            call.enqueue(new Callback<ATaxiList<Driver>>() {
                @Override
                public void onResponse(Call<ATaxiList<Driver>> call, Response<ATaxiList<Driver>> response) {
                    Log.i(TAG, "Polled nearby drivers");
                    nearbyDrivers = response.body().getItems();
                    updateMap();
                }

                @Override
                public void onFailure(Call<ATaxiList<Driver>> call, Throwable t) {
                    Log.i(TAG, "Failed to poll nearby drivers");
                    t.printStackTrace();
                }
            });

            timerHandler.postDelayed(this, 1000 * 10);

        }
    };

    private void updateMap() {

        int primaryColor = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        Drawable taxiDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.taxi);
        taxiDrawable.setColorFilter(new LightingColorFilter(primaryColor, 1));
        BitmapDescriptor taxiMarker = BitmapDescriptorFactory.fromBitmap(drawableToBitmap(taxiDrawable));

        for (Marker marker : mDriverMarkers) {
            marker.remove();
        }

        for (Driver driver : nearbyDrivers) {
            if(mGmap != null){
                Marker marker = mGmap.addMarker(new MarkerOptions()
                    .position(driver.getLocation().getLatLng())
                    .icon(taxiMarker)
                );
                mDriverMarkers.add(marker);
            }
        }
    }

    public MainMapFragment() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainMapFragment.
     */
    public static MainMapFragment newInstance() {
        MainMapFragment fragment = new MainMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        FrameLayout child_fragment_container = (FrameLayout) v.findViewById(R.id.child_fragment_container);
        if(getChildFragmentManager().findFragmentByTag("map_tag") == null){
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.child_fragment_container, new SupportMapFragment(), "map_tag")
                    .commit();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mapFragment == null) { // Prevent resetting mapFragment from the backstack.
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentByTag("map_tag");
            mapFragment.onResume();
            mapFragment.getMapAsync(this);
        }

        timerHandler.postDelayed(timerRunnable, 0);

    }

    @Override
    public void onPause() {
        super.onPause();
        timerHandler.removeCallbacks(timerRunnable);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap gmap) {

        mGmap = gmap;

        timerHandler.postDelayed(timerRunnable, 0);

        List<LatLng> coords = new ArrayList<>();
        coords.add(new LatLng(41.6067308, 2.2862077));
        coords.add(new LatLng(41.6097000, 2.2899200));
        coords.add(new LatLng(41.6090412, 2.2851777));

        LatLng pos = new LatLng(41.60797, 2.28765);

        mMapSettings = gmap.getUiSettings();
        mMapSettings.setMapToolbarEnabled(false);
        mMapSettings.setMyLocationButtonEnabled(false);
        mMapSettings.setZoomControlsEnabled(false);

        int primaryColor = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        int accentColor = ContextCompat.getColor(getContext(), R.color.colorAccent);

        Drawable taxiDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.taxi);
        Drawable hereDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.map_marker);
        taxiDrawable.setColorFilter(new LightingColorFilter(primaryColor, 1));
        hereDrawable.setColorFilter(new LightingColorFilter(accentColor, 1));
        BitmapDescriptor taxiMarker = BitmapDescriptorFactory.fromBitmap(drawableToBitmap(taxiDrawable));
        BitmapDescriptor hereMarker = BitmapDescriptorFactory.fromBitmap(drawableToBitmap(hereDrawable));

        for (LatLng coord : coords) {
            Marker marker = gmap.addMarker(new MarkerOptions()
                    .position(coord)
                    .anchor(0.5f, 0.5f)
                    .icon(taxiMarker));

            mDriverMarkers.add(marker);
        }

        gmap.addMarker(new MarkerOptions()
                .position(pos)
                .anchor(0.5f, 1f)
                .icon(hereMarker));
//                .icon(BitmapDescriptorFactory.defaultMarker(4F)));

        gmap.animateCamera(CameraUpdateFactory.
                newCameraPosition(new CameraPosition(pos, 15, 0, 0)));
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
