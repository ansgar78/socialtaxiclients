package tk.ansgar.socialtaxiclients;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DriverListFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView mDriverListRecycler;
    private LinearLayoutManager llm;
    private DriverListAdapter mDriverListAdapter;

    public DriverListFragment() {
        // Required empty public constructor
    }

    public static DriverListFragment newInstance() {
        DriverListFragment fragment = new DriverListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_driver_list, container, false);

        mDriverListRecycler = (RecyclerView) v.findViewById(R.id.driver_list_recycler);

        llm                 = new LinearLayoutManager(getContext());
        mDriverListAdapter  = new DriverListAdapter(getResources());

        mDriverListAdapter.updateDriverList();

        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mDriverListRecycler.setHasFixedSize(true);
        mDriverListRecycler.setLayoutManager(llm);
        mDriverListRecycler.setAdapter(mDriverListAdapter);

        return v;
    }

//    private List<Driver> mockDrivers(int i) {
//        List<Driver> mock = new ArrayList<>();
//
//        for (; i>=0; i--){
//            mock.add(new Driver(
//                    "507c7f79bcf86cd7994f6c0e",
//                    "Mariano" + i,
//                    "Gutierrez",
//                    "fantasmeopunk@gmail.com",
//                    "555-555-555",
//                    Driver.Status.IN_RUN,
//                    new LatLng(
//                            41.0,
//                            -2.0
//                    )
//            ));
//        }
//
//        return mock;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
