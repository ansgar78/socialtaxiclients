package tk.ansgar.socialtaxiclients;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tk.ansgar.socialtaxiclients.models.ATaxiEndpointsInterface;
import tk.ansgar.socialtaxiclients.models.User;

public class ATaxi {
    private static ATaxi ourInstance = new ATaxi();

    private String sessionToken;
    private String googleId;
    private String googleTok;
    private User user;

    private Context context;

    private static final String BASE_URL = "http://api.ataxi.tk/";

    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new AuthHeadersInterceptor())
            .addInterceptor(new AuthInterceptor())
            .addInterceptor(new StethoInterceptor())
            .build();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    private final ATaxiEndpointsInterface endpoint = retrofit.create(ATaxiEndpointsInterface.class);

    public static ATaxi getInstance() {
        return ourInstance;
    }

    private ATaxi() {}

    public String getSessionToken() {
        return sessionToken;
    }
    public String getGoogleId() {
        return googleId;
    }
    public String getGoogleTok() {
        return googleTok;
    }
    public User getUser() {
        return user;
    }
    public Context getContext() {
        return context;
    }
    public ATaxiEndpointsInterface getApi() {
        return endpoint;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
    public void setGoogleTok(String googleTok) {
        this.googleTok = googleTok;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
