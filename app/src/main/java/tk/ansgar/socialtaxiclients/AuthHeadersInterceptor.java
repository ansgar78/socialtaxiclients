package tk.ansgar.socialtaxiclients;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class AuthHeadersInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(addHeaders(chain.request()));
    }

    public static Request addHeaders(Request request){
        if(ATaxi.getInstance().getSessionToken() != null) {
            return request.newBuilder()
                    .addHeader("X-Auth-Token", ATaxi.getInstance().getSessionToken())
                    .addHeader("X-Auth-UserId", ATaxi.getInstance().getGoogleId())
                    .build();
        } else {
            return request;
        }
    }
}