package tk.ansgar.socialtaxiclients;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.facebook.stetho.Stetho;


public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        OnFragmentInteractionListener
{

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavView;
    private FrameLayout mContentFrame;
    private ImageView mProfilePic;
    private ImageView mDrawerToggle;

    private FragmentManager mFragmentManager;
    private Fragment mMapFragment;
    private Fragment mRunCalcFragment;
    private Fragment mDriverListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);

        ATaxi.getInstance().setContext(getApplicationContext());

        // Force login
        startActivity(new Intent(this, LoginActivity.class));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavView = (NavigationView) findViewById(R.id.nav_view);
        mContentFrame = (FrameLayout) findViewById(R.id.content_frame);
        mDrawerToggle = (ImageView) findViewById(R.id.drawer_toggle);
        mProfilePic = (ImageView) mNavView.getHeaderView(0).findViewById(R.id.profile_pic);
        mFragmentManager = getSupportFragmentManager();

        Bitmap stockUserPhoto = BitmapFactory.decodeResource(getResources(), R.mipmap.stock_user_profile);

        assert mDrawerLayout != null;
        assert mNavView != null;
        assert mContentFrame != null;
        assert mDrawerToggle != null;
        assert mProfilePic != null;

        mProfilePic.setImageDrawable(getRoundedBitmap(getResources(), stockUserPhoto));

        mDrawerLayout.setStatusBarBackgroundColor(
                ContextCompat.getColor(
                        getApplicationContext(),
                        R.color.colorPrimary));

        mNavView.setNavigationItemSelectedListener(this);
        mNavView.setCheckedItem(R.id.nav_map);

        mMapFragment = MainMapFragment.newInstance();

//        changeFragment(mMapFragment);
        mFragmentManager.beginTransaction()
                .replace(R.id.content_frame, mMapFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.nav_map:
                if(mMapFragment == null)
                    mMapFragment = MainMapFragment.newInstance();
                changeFragment(mMapFragment);
                break;
            case R.id.nav_run_calc:
                if(mRunCalcFragment == null)
                    mRunCalcFragment = RunCalcFragment.newInstance();
                changeFragment(mRunCalcFragment);
                break;
            case R.id.nav_drivers:
                if(mDriverListFragment == null)
                    mDriverListFragment = DriverListFragment.newInstance();
                changeFragment(mDriverListFragment);
                break;
            case R.id.nav_info:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeFragment(Fragment fragment){

        if(!(mFragmentManager.findFragmentById(R.id.content_frame).getClass().equals(fragment.getClass()))){
            mFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public static RoundedBitmapDrawable getRoundedBitmap(Resources res, Bitmap bitmap) {
        RoundedBitmapDrawable roundBitMap = RoundedBitmapDrawableFactory.create(res, bitmap);
        roundBitMap.setCircular(true);
        roundBitMap.setAntiAlias(true);
        return roundBitMap;
    }

    public void toggleDrawer(View view) {
        if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}
